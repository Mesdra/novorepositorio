package udc.edu.lista;

public interface Iterador {
	public abstract Object dado();
	public abstract Object proximo();
	public abstract Object anterior();
	public abstract boolean temProximo();
	public abstract boolean temAnterior();
	public abstract int insereAntes(Object obj);
	public abstract int insereDepois(Object obj);
}
