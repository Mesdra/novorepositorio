package udc.edu.lista;

public class ListaDupEncT implements Lista {
	private class No{
		public No anterior;
		public No proximo;
		public Object dado;
		
		public No(No proximo, No anterior, Object dado)
		{
			this.anterior = anterior;
			this.proximo = proximo;
			this.dado = dado;
		}
		public No(Object dado)
		{
			this.anterior = null;
			this.proximo = null;
			this.dado = dado;
		}
	}
	private class IteradorLista implements Iterador
	{
		public No noAtual;
		
		public IteradorLista(No no)
		{
			noAtual = no;
		}
		
		@Override
		public Object dado() {
			// um acesso ao noAtual
			numAcessosTotalNos++;
			return noAtual.dado;
		}

		@Override
		public Object proximo() {
			if(noAtual == null) 
				return null;
			// um acesso ao noAtual
			numAcessosTotalNos++;

			Object obj = noAtual.dado;
			noAtual = noAtual.proximo;
			return obj;
		}

		@Override
		public Object anterior() {
			if(noAtual == null) 
				return null;
			// um acesso ao noAtual
			numAcessosTotalNos++;

			Object obj = noAtual.dado;
			noAtual = noAtual.anterior;
			return obj;
		}

		@Override
		public boolean temProximo() {
			// um acesso ao noAtual
			numAcessosTotalNos++;
			return noAtual.proximo != null ? true : false;
		}

		@Override
		public boolean temAnterior() {
			// um acesso ao noAtual
			numAcessosTotalNos++;
			return noAtual.anterior != null ? true : false;
		}

		@Override
		public int insereAntes(Object obj) {
			if(noAtual == null)
				return tamanho;
			
			// um acesso ao noAtual
			numAcessosTotalNos++;
			No no = new No(noAtual, noAtual.anterior, obj);
			noAtual.anterior.proximo = no;
			noAtual.anterior = no;
			return ++tamanho;
		}

		@Override
		public int insereDepois(Object obj) {
			if(noAtual == null)
				return tamanho;
			
			// um acesso ao noAtual
			numAcessosTotalNos++;
			No no = new No(noAtual.proximo, noAtual, obj);
			noAtual.proximo.anterior = no;
			noAtual.proximo = no;
			return ++tamanho;
		}
		
	}
	
	private No inicio;
	private No fim;

	private int tamanho;
	
	private int numAcessosTotalNos;

	public ListaDupEncT() {
		inicio = null;
		fim = null;
		tamanho = 0;
		numAcessosTotalNos = 0;
	}
	
	public int tamanho()
	{
		return tamanho;
	}
	
	public int numTotalAcessos()
	{
		return numAcessosTotalNos;
	}

	public Iterador inicio()
	{
		return new IteradorLista(inicio);
	}
	
	public Iterador fim()
	{
		return new IteradorLista(fim);
	}
	
	public int insereInicio(Object obj)
	{
		return insere(obj, 0);
	}
	public int insereFim(Object obj)
	{
		return insere(obj, tamanho);
	}
	public int insere(Object obj, int pos) {
		No no;

		if (obj == null)
			return tamanho;

		if (inicio == null)// primeiro elemento - lista vazia
		{
			no = new No(obj);
			inicio = fim = no;
			
			// um acesso ao novo n�
			numAcessosTotalNos++;
		} else // j� existem elementos na lista
		{
			if (pos <= 1)// inserir no inicio da lista
			{
				no = new No(inicio, null, obj);
				inicio.anterior = no;
				inicio = no;
				// um acesso ao novo n�
				numAcessosTotalNos++;
			} else if (pos >= tamanho)// inserir no final da lista
			{
				no = new No(null, fim, obj);
				fim.proximo = no;
				fim = no;
				// um acesso ao novo n�
				numAcessosTotalNos++;
			} else// inserir no meio da lista
			{
				No aux = inicio;
				while (pos > 0) {
					aux = aux.proximo;
					pos--;
					// um acesso ao n� aux
					numAcessosTotalNos++;
				}
				// inserir na posi��o de aux
				no = new No(aux, aux.anterior, obj);
				aux.anterior.proximo = no;
				aux.anterior = no;
				// um acesso ao novo n�
				numAcessosTotalNos++;
			}
		}
		tamanho++;
		return tamanho;
	}

	public Object removeInicio()
	{
		return remove(0);
	}
	public Object removeFim()
	{
		return remove(tamanho);
	}
	public Object remove(int pos) {
		Object obj;
		if (inicio == null)// se a lista est� vazia
			return null;
		if (inicio == fim)// se existe apenas um elemento na lista
		{
			obj = inicio.dado;
			inicio = fim = null;
			tamanho--;
			// um acesso ao n� inicio
			numAcessosTotalNos++;
			return obj;
		}
		if (pos <= 1)// remover o primeiro elemento da lista
		{
			obj = inicio.dado;
			inicio = inicio.proximo;
			inicio.anterior = null;
			// um acesso ao n� inicio
			numAcessosTotalNos++;
		} else if (pos >= tamanho)// remover o �ltimo elemento da lista
		{
			obj = fim.dado;
			fim = fim.anterior;
			fim.proximo = null;
			// um acesso ao n� fim
			numAcessosTotalNos++;
		} else // remover um elemento no meio da lista
		{
			No aux = inicio;
			while (pos > 0) {
				aux = aux.proximo;
				pos--;
				// um acesso ao n� aux
				numAcessosTotalNos++;
			}
			// remover o elemento aux
			obj = aux.dado;
			aux.anterior.proximo = aux.proximo;
			aux.proximo.anterior = aux.anterior;
		}
		tamanho--;
		return obj;
	}

	public Object consulta(int pos) {
		if (inicio == null)// se a lista est� vazia
			return null;
		if (inicio == fim)// se existe apenas um elemento na lista
		{
			// um acesso ao n� inicio
			numAcessosTotalNos++;
			return inicio.dado;
		}
		if (pos == 0)// consulta o primeiro elemento da lista
		{
			// um acesso ao n� inicio
			numAcessosTotalNos++;
			return inicio.dado;
		}
		else if (pos >= tamanho - 1)// consulta o �ltimo elemento da lista
		{
			// um acesso ao n� fim
			numAcessosTotalNos++;
			return fim.dado;
		}
		else // consulta um elemento no meio da lista
		{
			No aux = inicio;
			while (pos > 0) {
				aux = aux.proximo;
				pos--;
				// um acesso ao n� aux
				numAcessosTotalNos++;
			}
			// consulta o elemento aux
			return aux.dado;
		}
	}
}
