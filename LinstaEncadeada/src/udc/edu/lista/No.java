package udc.edu.lista;

public class No {
	private No proximo;
	private No anterior;
	private Object dado;
	
	//Construtor completamenta especificado
	public No(No proximo, No anterior, Object dado) {
		this.proximo = proximo;
		this.anterior = anterior;
		this.dado = dado;
	}
	
	public No(Object dado) {
		this.proximo = null;
		this.anterior = null;
		this.dado = dado;
	}
	
	//Construtor padr�o
	public No() {
		this.proximo = null;
		this.anterior = null;
		this.dado = null;
	}

	public No getProximo() {
		return proximo;
	}

	public void setProximo(No proximo) {
		this.proximo = proximo;
	}

	public No getAnterior() {
		return anterior;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	public Object getDado() {
		return dado;
	}

	public void setDado(Object dado) {
		this.dado = dado;
	}

}